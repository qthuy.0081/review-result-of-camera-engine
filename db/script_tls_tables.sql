
/****** Object:  Table [dbo].[TLS_InTransactionHistory]    Script Date: 8/11/2020 4:36:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TLS_InTransactionHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[SmartCardId] [varchar](15) NULL,
	[StationId] [varchar](2) NOT NULL,
	[LaneId] [varchar](4) NOT NULL,
	[ShiftId] [varchar](6) NOT NULL,
	[CheckDate] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[TransactionId] [varchar](25) NOT NULL,
	[FrontPlate] [varchar](15) NULL,
	[RearPlate] [varchar](15) NULL,
	[ManualPlate] [varchar](15) NULL,
	[ImageExtension] [varchar](5) NOT NULL,
	[F1] [int] NOT NULL,
	[F2] [int] NOT NULL,
	[F3] [int] NOT NULL,
	[N1] [varchar](50) NULL,
	[N2] [varchar](50) NULL,
	[N3] [varchar](50) NULL,
	[TransactionType] [int] NOT NULL,
 CONSTRAINT [PK_TLS_InTransactionHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TLS_InTransactionHistory] ADD  DEFAULT ((0)) FOR [F1]
GO

ALTER TABLE [dbo].[TLS_InTransactionHistory] ADD  DEFAULT ((0)) FOR [F2]
GO

ALTER TABLE [dbo].[TLS_InTransactionHistory] ADD  DEFAULT ((0)) FOR [F3]
GO

ALTER TABLE [dbo].[TLS_InTransactionHistory] ADD  DEFAULT ((0)) FOR [TransactionType]
GO



/****** Object:  Table [dbo].[TLS_InTransaction]    Script Date: 8/11/2020 4:36:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TLS_InTransaction](
	[Id] [uniqueidentifier] NOT NULL,
	[SmartCardId] [varchar](15) NULL,
	[StationId] [varchar](2) NOT NULL,
	[LaneId] [varchar](4) NOT NULL,
	[ShiftId] [varchar](6) NOT NULL,
	[TransactionId] [varchar](25) NOT NULL,
	[CheckDate] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[FrontPlate] [varchar](15) NULL,
	[RearPlate] [varchar](15) NULL,
	[ManualPlate] [varchar](15) NULL,
	[ImageExtension] [varchar](5) NOT NULL,
	[TransactionType] [int] NOT NULL,
	[F1] [int] NOT NULL,
	[F2] [int] NOT NULL,
	[F3] [int] NOT NULL,
	[N1] [varchar](50) NULL,
	[N2] [varchar](50) NULL,
	[N3] [varchar](50) NULL,
 CONSTRAINT [PK_TLS_InTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TLS_InTransaction] ADD  CONSTRAINT [DF__TLS_InTra__Stati__12C8C788]  DEFAULT ('') FOR [StationId]
GO

ALTER TABLE [dbo].[TLS_InTransaction] ADD  DEFAULT ((0)) FOR [TransactionType]
GO

ALTER TABLE [dbo].[TLS_InTransaction] ADD  DEFAULT ((0)) FOR [F1]
GO

ALTER TABLE [dbo].[TLS_InTransaction] ADD  DEFAULT ((0)) FOR [F2]
GO

ALTER TABLE [dbo].[TLS_InTransaction] ADD  DEFAULT ((0)) FOR [F3]
GO


